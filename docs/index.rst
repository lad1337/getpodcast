.. getpodcast documentation master file, created by
   sphinx-quickstart on Sat Sep  3 08:02:43 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to getpodcast's documentation!
======================================

* PyPI: https://pypi.org/project/getpodcast/
* Documentation: https://fholmer.gitlab.io/getpodcast
* Source code: https://gitlab.com/fholmer/getpodcast
* License: MIT

Summary
=======

Simplify downloading podcasts with getpodcast

Features
========

* No database or caching backend. RSS items are instead compared to file name,
  file size and file timestamp

* Configuration and hooks are stored in a single python file, which is also
  executable

User guide
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   01_user_guide/*
   02_dev_guide/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
