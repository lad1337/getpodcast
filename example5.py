#! /usr/bin/env python3

"""
* Set ``date_from`` to the last 30 days using the `datetime`-module.
* Enable the ``deleteold`` option. Episodes older than 30 days will
  now be deleted.
"""

import datetime

import getpodcast

fromdate = datetime.date.today() - datetime.timedelta(days=30)

opt = getpodcast.options(
    date_from=fromdate.isoformat(),
    deleteold=True,
    user_agent="Mozilla/5.0",
    root_dir="./podcast",
    template="{rootdir}/{podcast}/{year}/{date} - {title}{ext}",
)

podcasts = {
    "LAN": "https://linuxactionnews.com/rss",
}

getpodcast.getpodcast(podcasts, opt)
